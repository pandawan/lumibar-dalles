MCU=atmega328p
DUDECPUTYPE=m328p

PROGRAMER=stk500v2
LOADCMD=avrdude
LOADARG=-P /dev/ttyUSB1 -p $(DUDECPUTYPE) -c $(PROGRAMER) -e -U flash:w:

CC=avr-gcc
OBJCOPY=avr-objcopy

# optimize for size:
CFLAGS=-mmcu=$(MCU) -Wall -Wstrict-prototypes -Os -mcall-prologues -std=c99
#-------------------
.PHONY: all fuse
#-------------------
all: dalles.hex
#
fuse: wrfuse8mhz
#-------------------
help:
	@echo "Usage: make all|load|load_pre|rdfuses"
	@echo "The Makefile has also fuse settings for atmega8 in the code."
	@echo "Do not use those make targets for any other chip than atmega8."
	@echo "The atmega8 fuse targest are: wrfuse1mhz|wrfuse4mhz|wrfuse8mhz|wrfusecrystal"
	@echo "Warning: you will not be able to undo wrfusecrystal unless you connect an"
	@echo "         external crystal! The uC is dead after wrfusecrystal if you do"
	@echo "         not have an external crystal."
#-------------------
dalles.hex : dalles.out
	avr-size dalles.out
	$(OBJCOPY) -R .eeprom -O ihex dalles.out dalles.hex
dalles.out : dalles.o
	$(CC) $(CFLAGS) -o dalles.out -Wl,-Map,dalles.map dalles.o
dalles.o : dalles.c
	$(CC) $(CFLAGS) -Os -c dalles.c
#------------------
load: dalles.hex
	$(LOADCMD) $(LOADARG)dalles.hex
#-------------------
clean:
	rm -f *.o *.map *.out dalles.hex
#-------------------
