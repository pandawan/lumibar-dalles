#define F_CPU 1000000UL  // 1 MHz

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

// Const used for serial communication
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE ((((F_CPU / 16) + (USART_BAUDRATE / 2)) / (USART_BAUDRATE)) - 1)

#define IO_BL DDRD
#define PORT_BL PORTD
#define PORT_BL_RED_PIN 7
#define PORT_BL_GREEN_PIN 4
#define PORT_BL_BLUE_PIN 3

#define IO_BR DDRC
#define PORT_BR PORTC
#define PORT_BR_RED_PIN 0
#define PORT_BR_GREEN_PIN 1 
#define PORT_BR_BLUE_PIN 2

#define IO_TL DDRD
#define PORT_TL PORTD
#define PORT_TL_RED_PIN 5
#define PORT_TL_BLUE_PIN 2


// /!\ Pas le même port pour le vert sur ce coin là
#define IO_TL_SUP DDRB
#define PORT_TL_SUP PORTB
#define PORT_TL_SUP_GREEN_PIN 7 

#define IO_TR DDRC
#define PORT_TR PORTC
#define PORT_TR_RED_PIN 3
#define PORT_TR_GREEN_PIN 4
#define PORT_TR_BLUE_PIN 5

#define BOTTOM_LEFT 0
#define BOTTOM_RIGHT 1
#define TOP_LEFT 2
#define TOP_RIGHT 3

#define FRAME_PER_COLOR 1

int currentColor[12] = { 0,0,0, 0,0,0, 0,0,0, 0,0,0 };
int lastColor[12] = { 0,0,0, 0,0,0, 0,0,0, 0,0,0 };
int nextColor[12] = { 0,0,0, 0,0,0, 0,0,0, 0,0,0 };
int gradStep=0, a, b;
int stepsInGradients = 100;


// _delay_ms uses a floating point datatype if you call
// that function in many places in your code then it becomes
// very fat. An integer is enough for us:
//
// delay x milliseconds:

void delay_ms(unsigned int xms) {
    while(xms){
        _delay_ms(0.96);
        xms--;
    }
}

void oldChangeColor(void) {
    for(int i=0; i<12; i++) {
        if(i%3!=1) {
            lastColor[i] = currentColor[i];
            nextColor[i] = 255-currentColor[i];
        }
    }
    gradStep = 0;
}

void changeColor(
    int corner,
    int gradient,
    int red,
    int green, 
    int blue) {
    for(int i = 0; i<3; i++) {
        lastColor[corner*3+i] = currentColor[corner*3+i];
    }
    nextColor[corner*3] = 0;
    nextColor[corner*3+1] = 0;
    nextColor[corner*3+2] = 0;

    gradStep = 0;
}

int max(int a, int b) {
    if(a<b)
        return b;
    return a;
}

void nextFrame(void) {
    int t;

    int timePosition;
    for(t = 0; t<256; t+=14) {
        timePosition = t % 256;

        int output[12] = {
            currentColor[ 0],
            currentColor[ 1],
            currentColor[ 2],
            currentColor[ 3],
            currentColor[ 4],
            currentColor[ 5],
            currentColor[ 6],
            currentColor[ 7],
            currentColor[ 8],
            currentColor[ 9],
            currentColor[10],
            currentColor[11]
        };
        
        for(int i=0; i<9; i++) {
            output[i] = output[i] > timePosition;
        }
        
        PORT_BL = 0xFF & ((output[0] << PORT_BL_RED_PIN)
                + (output[1] << PORT_BL_GREEN_PIN)
                + (output[2] << PORT_BL_BLUE_PIN));
        PORT_BR = 0xFF & ((output[0] << PORT_BR_RED_PIN)
                + (output[1] << PORT_BR_GREEN_PIN)
                + (output[2] << PORT_BR_BLUE_PIN));
        PORT_TL = 0xFF & ((output[0] << PORT_TL_RED_PIN)
                + (output[2] << PORT_TL_BLUE_PIN));
        PORT_TL_SUP = 0xFF & (output[1] << PORT_TL_SUP_GREEN_PIN);
        PORT_TR = 0xFF & ((output[0] << PORT_TR_RED_PIN)
                + (output[1] << PORT_TR_GREEN_PIN)
                + (output[2] << PORT_TR_BLUE_PIN));
    }
    _delay_ms(1);
}

int main(void) {
    // *** INITIALIZATION ***
    // enable pins as output
    IO_BL|= (1 << PORT_BL_RED_PIN) + (1 << PORT_BL_GREEN_PIN) + (1 << PORT_BL_RED_PIN);
    IO_BR|= (1 << PORT_BR_RED_PIN) + (1 << PORT_BR_GREEN_PIN) + (1 << PORT_BR_RED_PIN);
    IO_TR|= (1 << PORT_TR_RED_PIN) + (1 << PORT_TR_GREEN_PIN) + (1 << PORT_TR_RED_PIN);
    IO_TL|= (1 << PORT_TL_RED_PIN) + (1 << PORT_TL_RED_PIN);
    IO_TL_SUP|= (1 << PORT_TL_SUP_GREEN_PIN);

    // --- Serial set up ---
    // Turn on the reception and transmission (last is use for debug)
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);
    // Use 8-bit character size
    UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);

    UBRR0H = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register
    UBRR0L = BAUD_PRESCALE; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register

    // Set up for interrupt driven serial communication
    // Enable interruption for message reception in a serial communication
    UCSR0B |= (1 << RXCIE0);
    // Enable interruptions
    sei();

    // --- SET GRADIENT DIRECTION ---
    for(int c=0; c<12; c++) {
        nextColor[c]= c%3==0 ? 0 : 255;
    }

    // *** MAIN LOOP ***
    gradStep = 0;
    while(1) {
        if(gradStep<stepsInGradients) {
            gradStep++;
        } else {
            gradStep = stepsInGradients;
        }
        a = stepsInGradients-gradStep;
        b = gradStep;
        for(int corner=0; corner<4; corner++) {
            for(int comp=0; comp<3; comp++) {
                currentColor[corner*3 + comp] = max(
                    (lastColor[corner*3 + comp] * a
                    + nextColor[corner*3 + comp] * b) / stepsInGradients, 0);
            }
        }

        for(int i=0; i<FRAME_PER_COLOR; i++) {
            nextFrame();
        }
    }
    
    return(0);
}


#define START_BYTE 40 //random value
#define END_BYTE 41 //random value
int cornerDescribed=0;
int nextGradient, nextRed, nextGreen, nextBlue;

int readByte(void) {
    return (UDR0+128) % 256;
}

int waitAndReadByte(void) {
    while ((UCSR0A & (1 << RXC0)) == 0) {}; 
    return readByte();
}

// Interruption for serial reception
// = code that will be triggered when a message is received
ISR(USART_RX_vect)
{/*
    int receivedByte = readByte();

    if(receivedByte == START_BYTE) {
        cornerDescribed = waitAndReadByte() % 4; // id
        nextGradient = waitAndReadByte(); // gradient
        nextRed = waitAndReadByte(); // red
        nextGreen = waitAndReadByte(); // green
        nextBlue = waitAndReadByte(); // blue
        receivedByte = waitAndReadByte();
        if(receivedByte == START_BYTE) {
            changeColor(0, 100, 100, 100, 100);
//            changeColor(cornerDescribed, nextGradient, nextRed, nextGreen, nextBlue);
        } else {
            //oldChangeColor();
        }
    }
*/
    changeColor(0,100,100,100,100);
}
