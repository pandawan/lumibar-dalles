/*********************************************
* vim: set sw=8 ts=8 si et :
* Author: Guido Socher, Copyright: GPL
* This program is to test the led connected to LEDOUT (see below)
*
* Clock frequency     : Internal clock 1 Mhz (factory default)
*********************************************/

#define F_CPU 1000000UL  // 1 MHz

#include <avr/io.h>
#include <util/delay.h>

#define IO_BL DDRD
#define PORT_BL PORTD
#define PORT_BL_RED_PIN 7
#define PORT_BL_GREEN_PIN 4
#define PORT_BL_BLUE_PIN 3

#define IO_BR DDRC
#define PORT_BR PORTC
#define PORT_BR_RED_PIN 0
#define PORT_BR_GREEN_PIN 1 
#define PORT_BR_BLUE_PIN 2

#define IO_TL DDRD
#define PORT_TL PORTD
#define PORT_TL_RED_PIN 5
#define PORT_TL_GREEN_PIN 6 //7 sur PB7 :/ 
#define PORT_TL_BLUE_PIN 2

#define IO_TR DDRC
#define PORT_TR PORTC
#define PORT_TR_RED_PIN 3
#define PORT_TR_GREEN_PIN 4
#define PORT_TR_BLUE_PIN 5

#define BOTTOM_LEFT 0
#define BOTTOM_RIGHT 1
#define TOP_LEFT 2
#define TOP_RIGHT 3


int black[3] =      { 0,   0,   0   };
int white[3] =      { 255, 255, 255 };
int red[3] =        { 255, 0,   0   };
int green[3] =      { 0,   255, 0   };
int blue[3] =       { 0,   0,   255 };
int cyan[3] =       { 0,   255, 255 };
int magenta[3] =    { 255, 0,   255 };
int yellow[3] =     { 255, 255, 0   };

int *current_color[4] = { black, black, black, black };

// _delay_ms uses a floating point datatype if you call
// that function in many places in your code then it becomes
// very fat. An integer is enough for us:
//
// delay x milliseconds:

void delay_ms(unsigned int xms) {
    while(xms){
        _delay_ms(0.96);
        xms--;
    }
}


void nextFrame(void) {
    int t, red[4], green[4], blue[4];
    int i;
    for(i=0;i<4; i++) {
        red[i] = current_color[i][0];
        blue[i] = current_color[i][1];
        green[i] = current_color[i][2];
    }

    int timePosition, output[3];
    int mask;
    for(t = 0; t<256; t+=14) {
        timePosition = t % 256;
        for(i=0;i<4; i++) {
            output[0] = red[i] > timePosition;
            output[1] = green[i] > timePosition;
            output[2] = blue[i] > timePosition;
            switch (i) {
                case BOTTOM_LEFT:
                    mask = PORT_BL;
                    mask &= (0 << PORT_BL_RED_PIN)
                            + (0 << PORT_BL_GREEN_PIN)
                            + (0 << PORT_BL_BLUE_PIN);
                    mask |= (output[0] << PORT_BL_RED_PIN)
                            + (output[1] << PORT_BL_GREEN_PIN)
                            + (output[2] << PORT_BL_BLUE_PIN);
                    PORT_BL = mask;
                    break;
                case BOTTOM_RIGHT:
                    mask = PORT_BR;
                    mask &= (0 << PORT_BR_RED_PIN)
                            + (0 << PORT_BR_GREEN_PIN)
                            + (0 << PORT_BR_BLUE_PIN);
                    mask |= (output[0] << PORT_BR_RED_PIN)
                            + (output[1] << PORT_BR_GREEN_PIN)
                            + (output[2] << PORT_BR_BLUE_PIN);
                    PORT_BR = mask;
                    break;
                case TOP_LEFT:
                    mask = PORT_TL;
                    mask &= (0 << PORT_TL_RED_PIN)
                            + (0 << PORT_TL_GREEN_PIN)
                            + (0 << PORT_TL_BLUE_PIN);
                    mask |= (output[0] << PORT_TL_RED_PIN)
                            + (output[1] << PORT_TL_GREEN_PIN)
                            + (output[2] << PORT_TL_BLUE_PIN);
                    PORT_TL = mask;
                    break;
                case TOP_RIGHT:
                    mask = PORT_TR;
                    mask &= (0 << PORT_TR_RED_PIN)
                            + (0 << PORT_TR_GREEN_PIN)
                            + (0 << PORT_TR_BLUE_PIN);
                    mask |= (output[0] << PORT_TR_RED_PIN)
                            + (output[1] << PORT_TR_GREEN_PIN)
                            + (output[2] << PORT_TR_BLUE_PIN);
                    PORT_TR = mask;
                    break;
                default:
                    break;
            }
        }
    }
}


#define NB_STEP 255
void gradiant(
    int bl_c1[3], int bl_c2[3],
    int br_c1[3], int br_c2[3],
    int tl_c1[3], int tl_c2[3],
    int tr_c1[3], int tr_c2[3]
    ) {
    int i;
    long t;
    double a, b;
    for (t = 0; t<NB_STEP; t++) {
        a = NB_STEP - t;
        b = t;
        for(i=0; i<3; i++) {
            current_color[BOTTOM_LEFT][i]   = (bl_c1[i]*a + bl_c2[i]*b) / NB_STEP;
            current_color[BOTTOM_RIGHT][i]  = (br_c1[i]*a + br_c2[i]*b) / NB_STEP;
            current_color[TOP_LEFT][i]      = (tl_c1[i]*a + tl_c2[i]*b) / NB_STEP;
            current_color[TOP_RIGHT][i]     = (tr_c1[i]*a + tr_c2[i]*b) / NB_STEP;
        }
        nextFrame();
    }
}

#define PATTERN_SIZE 5
int main(void) {
    /* enable pin as output */
    IO_BL|= 0xFF;
    IO_BR|= 0XFF;
    IO_TR|= 0XFF;
    IO_TL|= 0XFF;

    PORT_TR = 0xFF;
    PORT_TL = 0xFF;
    PORT_BR = 0xFF;
    PORT_BL = 0xFF;

    while(1) {
        /*
        gradiant(
            0x0000FF, 0x00FF00,
            0x00FF00, 0xFF0000,
            0x0000FF, 0x00FF00,
            0x00FF00, 0xFF0000
        );
        gradiant(
            0x00FF00, 0xFF0000,
            0x0000FF, 0x00FF00,
            0x00FF00, 0xFF0000,
            0x0000FF, 0x00FF00
        );
        */

        gradiant(
            blue, blue,
            green, green,
            blue, blue,
            green, green
        );
        gradiant(
            blue, blue,
            green, green
            blue, blue,
            green, green
        );
    }

    return(0);
}

